# Sails.js, Angular.js, MongoDB example application (MEANS stack)

# Installation

Important: Please use sudo on Mac/Linux systems. It may not be required on Windows

(Clone the repository)
sudo git clone https://github.com/Rajan/sailsjs-angularjs-mongodb-example

(It was in read-only mode. Changing permissions here.)
sudo chmod -R 777 sailsjs-angularjs-mongodb-example/

cd sailsjs-angularjs-mongodb-example/

(Installing related node.js modules)
sudo npm install

Based on https://github.com/balderdashy/sails-angular-seed.
